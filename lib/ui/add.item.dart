import 'package:flutter/material.dart';
import 'package:lista_c/models/item.dart';

class AddItem extends StatelessWidget {
  final itemC = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text('Add item'),
      content: TextField(
        autofocus: true,
        controller: itemC,
      ),
      actions: [
        ElevatedButton(
          child: Text('Cancelar'),
          onPressed: () => Navigator.of(context).pop(),
        ),
        ElevatedButton(
          onPressed: () {
            final item = new Item(title: itemC.value.text);
            itemC.clear();
            Navigator.of(context).pop(item);
          },
          child: Text('Adicionar'),
        )
      ],
    );
  }
}
