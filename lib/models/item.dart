class Item {
  Item({required this.title, this.idDone = false});

  String title;
  bool idDone;
}